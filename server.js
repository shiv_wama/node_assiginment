const express = require('express');
const path = require('path')
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express();
app.use('/', express.static(path.join(__dirname, 'static')))
app.use(bodyParser.json())
app.post('/api/submit-emp-data', async (req, res) => {

    let gender = '';
    let id = 1;
    if (req.body.gender == 1)
        gender = 'Male';
    else
        gender = 'Female';


    let rawdata = fs.readFileSync('employee_data.json');
    let employees = JSON.parse(rawdata);
    if (employees.length > 0)
        id = employees.length + 1;

    console.log(employees.length);

    const employee = {
        _id: id,
        name: req.body.name,
        age: req.body.age,
        role: req.body.role,
        gender: gender,
    }


    let data = JSON.stringify(employee);
    if (employees.length > 0) {
        //append
        fs.appendFile("employee_data.json", data, function (err) {
            if (err) throw err;
            console.log('The "data to append" was appended to file!');
        });
    }
    else {
        //write
        fs.writeFile('employee_data.json', data, (err) => {
            if (err) throw err;
            res.json({ status: 'ok', msg: 'Successfully wrote file.' })
        });
    }




})







app.listen(8000, () => {
    console.log('Server is running at port 8000')
})